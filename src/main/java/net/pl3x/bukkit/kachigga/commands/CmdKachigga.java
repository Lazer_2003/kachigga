package net.pl3x.bukkit.kachigga.commands;

import net.pl3x.bukkit.kachigga.Main;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CmdKachigga implements CommandExecutor {
	private Main plugin;

	public CmdKachigga(Main plugin) {
		this.plugin = plugin;
	}

	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (!cs.hasPermission("kachigga.command.kachigga")) {
			cs.sendMessage(ChatColor.translateAlternateColorCodes('&', "&4You do not have permission for that command!"));
			plugin.log(cs.getName() + " was denied access to that command!");
			return true;
		}

		if (args.length > 0 && args[0].equalsIgnoreCase("reload")) {
			plugin.reloadConfig();
			cs.sendMessage(ChatColor.translateAlternateColorCodes('&', "&d" + plugin.getName() + " config has been reloaded."));
			plugin.log("&aReloaded configs");
			return true;
		}

		cs.sendMessage(ChatColor.translateAlternateColorCodes('&', "&d" + plugin.getName() + " v" + plugin.getDescription().getVersion()));
		return true;
	}
}
