package net.pl3x.bukkit.kachigga.listeners;

import net.pl3x.bukkit.kachigga.Main;
import net.pl3x.bukkit.kachigga.tasks.Thunder;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerListener implements Listener {
	private Main plugin;

	public PlayerListener(Main plugin) {
		this.plugin = plugin;
	}

	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
	public void onPlayerJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		boolean kachigga = false;

		if (player.hasPermission("kachigga.onjoin")) {
			Bukkit.getScheduler().runTaskLater(plugin, new Thunder(player), 10);
			kachigga = true;
		}

		event.setJoinMessage(getJoinMessage(player, kachigga));
	}

	private String getJoinMessage(Player player, boolean kachigga) {
		String message;

		if (kachigga) {
			message = plugin.getConfig().getString("custom-join-kachigga", "&e{player} says the forcast is 100% chance of thunder! Kachigga!");
		} else {
			message = plugin.getConfig().getString("custom-join-regular", "&e{player} joined the game.");
		}

		message = message.replace("{player}", player.getName());

		return ChatColor.translateAlternateColorCodes('&', message);
	}
}
